<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'count', 'mark','location'
    ];
    public function users(){
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
