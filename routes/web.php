<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Product;
use App\User;

Route::get('/', function () {
    //$user = User::find(2);
    //return view('welcome')->with('user',$user);
    $product = Product::find(3);
    return view('welcome')->with('product',$product);
});

Auth::routes();

Route::get('/home', 'HomeController@index');
